/**
 *
 * Given is the following FizzBuzz application which counts from 1 to 100 and outputs either the
 * corresponding number or the corresponding text if one of the following rules apply.
 * 
 * Rules:
 *  - dividable by 3 without a remainder -> Fizz
 *  - dividable by 5 without a remainder -> Buzz
 *  - dividable by 3 and 5 without a remainder -> FizzBuzz
 *
 * ACCEPTANCE CRITERIA:
 * 
 * Please refactor this code so that it can be easily extended with other rules, such as
 * - "if it is dividable by 7 without a remainder output Bar"
 * - "if multiplied by 10 is larger than 100 output Foo"
 */


/*

This is not the most efficient result that we can get in terms of performance, but it's the most readabale. scalable and maintanable approach. 
If we are concerned with performance than example solution would be the best for it.
P.S. it would be better organize this objects in different catalouges, but i left it as is so it 
would be easier to check.

*/

const rules = [
  {
    condition: (number) => number % 3 === 0,
    stringValue: "Fizz"
  },
  {
    condition: (number) => number % 5 === 0,
    stringValue: "Buzz"
  },
  {
    condition: (number) => number % 7 === 0,
    stringValue: "Bar"
  },
  {
    condition: (number) => number * 10 > 100,
    stringValue: "Foo"
  },

];

function applyRules(number) {
  let output = "";
  rules.forEach(rule => {
    if (rule.condition && rule.condition(number)) {
      output += rule.stringValue;
    }
  });
  // return string if exists else return number
  return output ? output : number;
}

function fizzbuzz(limit = 100) {
  if (typeof limit !== 'number' || limit < 1) {
    return;
  } 
  for (let i = 1; i <= limit; i++) {
    let output = applyRules(i);
    console.log(output);
  }
}

fizzbuzz();
