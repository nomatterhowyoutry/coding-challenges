/**

Print a pyramid!

    *
   ***
  *****
 *******
*********

ACCEPTANCE CRITERIA:

Write a script to output pyramid of given size to the console (with leading spaces).

*/

/*

This can be easily done via nested loops, which will result in O(n**2) complexity algorithm
But if we can find corelation beetwen iterator and quantity of '*' algorithm becomes O(n)
I don't believe this routine can be completed in constant time though.

*/

function pyramid(size = 5) {
  if (typeof size !== 'number' || size < 1) {
    return;
  }
  for (let i = 0; i < size; i++) {
    console.log(`${' '.repeat(size - i - 1)}${'*'.repeat((Math.floor(i * 2) + 1))}`);
  }
}

pyramid(5);
